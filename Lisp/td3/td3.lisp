(defun interspace(obje l)
( cond  
        ((null(cdr l))l)
        (t(append (list(car l)) (list obje) (interspace obje (cdr l))))
       
 )
)

(defun interspaceite(obje l)
(let ((f (list (car l))))
     (dolist(elem (cdr l))
        (setf f (append f (list obje)  (list elem)))
     )
     f
 )
)
