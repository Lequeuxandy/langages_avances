#include <iostream>
using namespace std;

class Point{
private:
	 int px;
	 int py;
public:
 Point();
 Point(const int,const int);
 Point(const Point &p1);
 void afficher()const;
 void cloner(const Point &p2);
 int getpx()const;
 int getpy() const;
 Point& operator=(const Point&); 
 ~Point();
 
 
};
Point::Point(){
	px=1;
	py=2;
}
Point::Point(const int x,const int y){
px=x;
py=y;
}
Point::Point(const Point &p1){     
	this->px=p1.getpx();
	this->py=p1.getpy();
}
void Point::afficher()const{
	cout<<"px="<<this->px<<endl;
	cout<<"py="<<this->py<<endl;
}
void Point::cloner(const Point &p2){
	this->px=p2.getpx();
	this->py=p2.getpy();
}
Point::~Point(){cout<<"appel du destructeur"<<endl;}
int Point::getpx()const{return this->px;}
int Point::getpy()const{return this->py;}

Point& Point::operator=(const Point& s){
	this->px=s.px;
	this->py=s.py;
	return *this;
}
 


