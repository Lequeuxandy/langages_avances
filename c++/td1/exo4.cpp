#include <iostream>
using namespace std;

class Point{
private:
	 int px;
	 int py;
public:
 Point();
 Point(const int,const int);
 Point(const Point &p1);
 void afficher()const;
 void cloner(const Point &p2);
 ~Point();
};
Point::Point():px(1),py(2){}
Point::Point(const int x,const int y):px(x),py(y){}
Point::Point(const Point &p1){     //dans quelle cas mettre ->????
	this->px=p1.px;
	this->py=p1.py;
}
void Point::afficher()const{
	cout<<"px="<<this->px<<endl;
	cout<<"py="<<this->py<<endl;
}
void Point::cloner(const Point &p2){
	this->px=p2.px;
	this->py=p2.py;
}
Point::~Point(){cout<<"appel du destructeur"<<endl;}
 
int main(){
	Point *p1 = new Point();
	Point *p2 = new Point(10,20);
	p1->afficher();
	p1->cloner(*p2);
	p1->afficher();
	return 0;
}

