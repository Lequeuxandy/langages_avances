#include <iostream>
#include <cstring>
#include <cmath>

using namespace std;


class CString{
private:
   char *str;
   int taille;
public:
  static int nbreChaine ; 
  CString();
  CString(char c);
  CString(char* c);
  CString(CString &);
  static int nbrChaines();
  CString& plus(const char c);
  ~CString();
  char* getString();
 CString& operator=(CString&);
 bool plusGrandQue(CString&);
 bool infOuEgale(CString&);
 CString& plusGrand(CString&);

};
CString::CString(){
	taille=0;
	str = new char[1];
	str[0]='\0';
	nbreChaine++;
	
}
	
CString::CString(char c){
	taille = 1;
	str = new char[2];
	str[0]=c;
	str[1]='\0';
	nbreChaine++;
}
CString::CString(char *c){
	taille = strlen(c);
	str = new char[taille+1];
	strcpy(str,c);
	str[taille+1]='\0';
	nbreChaine++;
}
 int CString::nbrChaines(){
	
    return CString::nbreChaine ;
}

CString::~CString(){delete[] str;}

CString& CString::plus(const char c){
	
	 char* tmp = new char[taille];
	 strcpy(tmp,str);
	 delete[] str;
	 str = new char[taille+1];
	 strcpy(str,tmp);
	 str[taille]=c;
	 str[taille+1]='\0';
	 taille++;
	 return *this;
 }
 char* CString::getString() { return str; }	
 
 CString& CString::operator=(CString& tmp){
	 delete[] str;
	 str = new char [strlen(tmp.getString())+1];
	 strcpy(str,tmp.getString());
	 taille = strlen(tmp.getString())+1;
	 return *this;
	 }
bool CString::plusGrandQue(CString& tmp){  //on admet que _str est plus grand en taille que tmp
	int i = 0;
	while(str[i]==tmp.str[i] && i <= strlen(str)){
    i++;
	}
	if (str[i]>tmp.str[i]) return true;
	else 
	return false;
} 
bool CString::infOuEgale(CString& tmp){  //on admet que _str est plus grand en taille que tmp
    if (str[0]<tmp.str[0]) return true;
	else 
	return false;
} 

CString& CString::plusGrand(CString& tmp){
	if(this->plusGrandQue(tmp))return *this;
	else return tmp;
}
	
ostream& operator<<(ostream& o, CString& s)
{
	char *str =  s.getString();
	o << str;
	
	return o;
}	 
	
    

int CString::nbreChaine; 
int main()
{
CString s1( "toto" ),
 s2( 'z' ),
s3;

 cout << "nbrChaines" << CString::nbrChaines() << endl ;
s3 = s1.plus( 'w' ) ;
cout << "s3 = " << s3.getString() << endl;
CString s4;
s4 = s3;
cout << "s4 = "<< s4.getString() << endl;
if( s1.plusGrandQue(s2) ) // si s1 > s2 au sens alphabétique
cout << "plus grand" << endl ;

if( s1.infOuEgale(s2) ) // si s1 <= s2 au sens alphabétique
cout << "plus petit" << endl ;

s3 = s1.plusGrand( s2 ) ;// retourner s1 si s1>s2, s2 sinon
cout << "s3 = "<< s3 << endl;
return 0;
}
