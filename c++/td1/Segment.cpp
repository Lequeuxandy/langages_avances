#include <iostream>
#include "Point.cpp"
using namespace std;
#include <cmath>

class Segment{
	private:
	   Point p1;
	   Point p2;
	public:
	Segment(Point const &p1,Point const &p2);
	Segment(Segment  &s1);
	Point getp1();
	Point getp2();
	bool estvertical();
	bool esthorizontal();
	~Segment();
	double longueur();
	bool estsurDiagonale();
};
Point Segment::getp1(){return p1;}

Point Segment::getp2(){return p2;}

Segment::Segment(Point const &p1,Point const &p2){  //:p1(p1),p2(p2)
	this->p1 = p1;
	this->p2 = p2;
}

Segment::Segment(Segment  &s1){
	this->p1 = s1.getp1();
	this->p2 = s1.getp2();
}

Segment::~Segment(){cout<<"appel destructeur du segment"<<endl;}

double Segment::longueur(){
	return sqrt(pow(p2.getpx()-p1.getpx(),2) + pow(p2.getpy()-p1.getpy(),2));
}

bool Segment::estvertical(){
	if (this->p1.getpx()==this->p2.getpx()){
		return true;
		}
  else return false;
}

bool Segment::esthorizontal(){
	if (this->p1.getpy()==this->p2.getpy()){
		return true;
		}
  else return false;
}

bool Segment::estsurDiagonale(){
	if (p1.getpx() == p1.getpy() && p2.getpx() == p2.getpy()){
		return true;
		}
  else return false;
}

int main(){
	Point *p1 = new Point();
	Point *p2 = new Point(10,20);
	Point *p3 = new Point(5,30);
	Segment *s1 = new Segment(*p2,*p3);
	if (s1->estvertical()) cout<<"est vertical"<<endl;
	p1->afficher();
	p1->cloner(*p2);
	p1->afficher();
	delete p1;
	delete p2;
	
	return 0;
}
	

