#ifndef POINTCOLOR_H
#define POINTCOLOR_H
#include <iostream>
#include "Point.h"
#include <string.h>
class Pointcolor:public Point{    /*pour finir utiliser virtual pour fonction definie ds classe mere et fille
protected:
	 std::string color;
public:
 Pointcolor();
 Pointcolor(int a,int b,std::string c);
 Pointcolor(Pointcolor &p1);
 void cloner(const Pointcolor &p2);
 std::string getcolor()const;
 Pointcolor& operator=(const Point&); 
 ~Pointcolor();
};

#endif
