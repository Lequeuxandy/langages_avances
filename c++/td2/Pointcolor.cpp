#include "Pointcolor.h"
using namespace std;

Pointcolor::Pointcolor():Point::Point(){
	color = "red";
}

Pointcolor::Pointcolor(int a,int b,std::string c):Point::Point(a,b){
	color = c;
}

Pointcolor::Pointcolor(Pointcolor& c):Point::Point(c.px,c.py){
	color = c.getcolor();

}

std::string Pointcolor::getcolor()const{return color;}

Pointcolor::~Pointcolor(){cout<<"appel du destructeur"<<endl;}

