#include <iostream>
#include <cstring>
#include <cmath>

using namespace std;


class Vecteur{
	private:
	   int _taille;
	   int *_p;
	public:
       Vecteur();
	   Vecteur(int);
	   Vecteur(Vecteur&);
	   int gettaille();
	   Vecteur& operator=(Vecteur&);
	   int operator[](int i);
	   Vecteur& operator+=(Vecteur&);
	   void afficher();
	   
	};
int Vecteur::gettaille(){return _taille;}

Vecteur::Vecteur(){
	_taille = 1;
	_p = new int[1];
	_p[0]=1;
}
Vecteur::Vecteur(int a){
	int i = 0;
	_taille=a;
	_p=new int[a];
	while(i<_taille){
		_p[i]=1;
		i++;
	}
}
Vecteur::Vecteur(Vecteur& v1){
	int i = 0;
	_taille=v1.gettaille();
	_p=new int[_taille];
	while(i<_taille){
		_p[i]=v1._p[i];
	}
}

Vecteur& Vecteur::operator=(Vecteur& v1){
	delete[] _p;
	_p=new int[v1.gettaille()];
	_taille = v1.gettaille();
	int i = 0 ;
	while(i<_taille){
		_p[i]=v1._p[i];
		i++;
	}
	return *this;
}
int Vecteur::operator[](int i)
{
	if(i >= gettaille() || i < 0)
		cerr << "Erreur indice depasse la taille du tableau." << endl;
	return _p[i];
}	
/* marche pas à coriger*/
Vecteur& Vecteur::operator+=(Vecteur& v1){  //a modifier par la suite pas optimal du tout 
 int i = 0;
 if(this->_taille>=v1.gettaille()){   //si mon vecte est plus grand que v1 pas besoin d'allocation;
   
   while(i<v1.gettaille()){
	  _p[i]+=v1._p[i];
	  i++;
     }
   return *this;
}
else {
	  int *tmp=new int[v1.gettaille()];
	  while(i<this->gettaille()){
	     tmp[i]=_p[i]+v1._p[i];
	     i++;
     }
     while (i<v1.gettaille()){
         tmp[i]=v1._p[i];
         i++;
      }
 delete []_p;
 _p=tmp;
 return *this;
}
}
      
   
void Vecteur::afficher(){
	int i = 0;
	while(i<_taille){
		cout <<"["<<i<<"]"<<_p[i]<<endl;
		i++;
		}
} 
int main(){
	Vecteur v(5);
	Vecteur v1(7);
	v+=v1;
	v.afficher();
	return 0;
}

/*=, [], +=, <<, >>*/
