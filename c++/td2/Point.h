#ifndef POINT_H
#define POINT_H
#include <iostream>

class Point{
protected:
	 int px;
	 int py;
public:
 Point();
 Point(const int,const int);
 Point(const Point &p1);
 void afficher()const;
 void cloner(const Point &p2);
 int getpx()const;
 int getpy() const;
 Point& operator=(const Point&); 
 ~Point();
 
 
};

#endif
