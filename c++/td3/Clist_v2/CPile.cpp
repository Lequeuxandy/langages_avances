#include "CPile.h"

using namespace std;

template<class T>
CPile<T>::CPile() {
    this.my_type = "CPile";
}
template<class T>
CPile<T>::~CPile() {}

template<class T>
CList<T>& CPile<T>::operator>(T& a) {
    if (this->content == NULL) {
        cout << "Pile vide" << endl;
        exit(1);
    }
    else {
        a = this->content->value;
        this->content = this->content->next;
        return *this;
    }
}
