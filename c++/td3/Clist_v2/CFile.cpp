#include "CFile.h"
using namespace std;

template<class T>
CFile<T>::CFile() {
    this->my_type = "CFile";
}
template<class T>
CFile<T>::~CFile() {}

template<class T>
CList<T>& CFile<T>::operator>(T& entier) {
    if (this->content == NULL) {
        cout << "File vide" << endl;
        exit(1);
    }
    else {
        Element<T>* tmp = this->content->next;
        if (tmp == NULL) {
            entier = this->content->value;
            this->content = NULL;
            delete tmp;
            return *this;
        }
        else {
            Element<T>* aux = tmp;
            while (tmp->next != NULL) {
                aux = tmp;
                tmp = tmp->next;
            }
            entier = tmp->value;
            aux->next = NULL;
            return *this;
        }
    }
}
