#include "CList.h"

using namespace std;

template<class T>
CList<T>::CList() {
    my_type = "CList";
    content = NULL;
}
template<class T>
CList<T>::~CList() {
    if (content != NULL) {
        while (content->next != NULL) {
            Element<T>* tmp = content;
            content = content->next;
            delete tmp;
        }
        delete content;
        content = NULL;
    }
}
template<class T>
CList<T>& CList<T>::operator<(T a) {
    Element<T>* tmp = new Element<T>;
    tmp->value = a;
    if (content == NULL) {
        content = tmp; 
        tmp->next = NULL;
    }
    else {
        tmp->next = content;
        content = tmp;
    }
    return *this;
}
template<class T>
CList<T>& CList<T>::operator>(T& a) {
    cout << " je ne dispose pas de la faculter de suprimer des element d'une liste." << endl;
    return *this;
}
template<class T>
T& CList<T>::operator[](T a){
    Element<T>* tmp = content;
    int compteur = 1;
    while (tmp != NULL && compteur < a) {
        tmp = tmp->next;
        compteur++;
    }
    if (tmp == NULL) {
        cout << "index of range" << endl;
        exit(1);
    }
    else return tmp->value;
}
template<class T>
ostream& operator<<(ostream& flux, const CList<T>& list) {
    flux << "Voici une liste de type " << list.my_type << ":";
    if (list.content == NULL) flux << "liste vide" << endl;
    else {
        Element<T>* tmp = list.content;
        while (tmp != NULL) {
            flux << " " << tmp->value;
            tmp = tmp->next;
        }
        flux <<endl;
    }
    return flux;
}

