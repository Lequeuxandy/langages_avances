#ifndef CPILE_H
#define CPILE_H

#include <stdio.h>
#include "CList.h"
template<class T>
class CPile: public CList<T> {
    public:
        CPile(){
			this->my_type = "CPile";
		}
        ~CPile(){};
        CList<T>& operator>(T& a){
			if (this->content == NULL) {
				cout << "Pile vide" << endl;
				exit(1);
			}
			else {
				a = this->content->value;
				this->content = this->content->next;
				return *this;
			}
         }
};

#endif 
