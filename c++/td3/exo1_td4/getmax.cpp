#include <iostream>
#include <string>
#include <stdlib.h>
using namespace std;

template<typename T>
T getMax(T& a,T& b){
	if (a<b) return b;
	else return a;
}


int main(){
int i=5, j=6,k;
float l=10.0,m=5.0, n;

k=getMax(i,j); //retourne max
n=getMax(l,m);

cout << k << endl;
cout << n << endl;

/*string s1("toto"), s2("atatata"), s3;
s3 = getMax(s1,s2);
cout << s3<<endl;*/

string r= "a";
string f= "b";
string tmp;
tmp = getMax(r,f);
cout<<tmp<<endl;


return 0;

}
