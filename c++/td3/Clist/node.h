#ifndef NODE_H
#define NODE_H
#include <iostream>

class Node {
 private:
	int _elem;
	
 public:
    Node *_nextNode;
    Node();
    Node(const int a);
    Node(Node& n);
    ~Node();
    int getElem();
    void setNode(Node& n);
    Node& getNode();
    Node& getNext();
    void setNext();
    
};
  
#endif
