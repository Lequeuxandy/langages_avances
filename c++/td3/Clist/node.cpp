#include "node.h"
using namespace std;

Node::Node(){
	_elem = 0;
	_nextNode = NULL;
}

Node::Node(const int a){
	_elem = a;
	_nextNode = NULL;
}


Node::~Node(){};

int Node::getElem(){return _elem;}
void Node::setNode(Node& a){_elem = a.getElem();}
Node& Node::getNext(){return *_nextNode;}
