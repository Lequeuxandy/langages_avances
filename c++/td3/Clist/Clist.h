#ifndef ClIST_H
#define CLIST_H
#include <iostream>

#include "node.h"


class CList{
	protected:
	  Node * _tete;     //pointe sur le premier element de la liste utile poour parcourir une miste(on commmence au debut
	public:
	CList();
	Node* getTete() const;
	~CList();
	void ajoutliste(int a);
	virtual CList& operator<(int a)=0;
	CList& operator>(int& a);
	void afficherliste();
	
};

	

#endif

/*void main(){
CPile pile;
CFile file;
CList* ptList = &file;
* ptList < 0 < 1; //empiler deux valeurs dans la file
cout << * ptList;
int i;
* ptList > i; //récupérer une valeur de la file dans i
cout << * ptList << " i=" << i;
ptList = &pile;
* ptList < 0 < 1; //empiler deux valeurs dans la pile
cout << *ptPile;
* ptList > i; //récupérer une valeur de la pile dans i
cout << * ptList << " i=" << i;
}
*/
