
#include <iostream>
#include <string>
#include <stdlib.h>
using namespace std;

/*utiliser fonction prof pour les types*/

template<class T>
class Pair{
	private:
		T _a;
		T _b;
    public:
    Pair(T& b ,T& c){
		_a=b;
		_b=c;
	}
	~Pair(){};
    T getMax(){
	if (_a<_b) return _b;
	else return _a;
    }
}; 

int main(){
	int i=5, j=6;
	float l=10.0,m=5.0;
	
	Pair<int>myInts (i,j);
	Pair<float>myFloats (l,m);
	
	cout<<myInts.getMax()<< endl;
	cout<<myFloats.getMax()<<endl;
	return 0;
}
